[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"
alias set-git-memo="git config user.name 'Mac Signer';git config user.email 'signer@mediamotion.ch'"
alias phpstorm="open -na "PhpStorm.app" --args "$@""
alias mixed-content-scan="~/.composer/vendor/bramus/mixed-content-scan/bin/mixed-content-scan"
alias flushhosts="sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder"
alias edithosts="sudo nano /etc/hosts"
alias trust-cert="sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain"
alias docker-exec="docker -it exec"
alias restart-apache="sudo apachectl -k restart"
alias turn-off-start-sound="sudo nvram -d SystemAudioVolume"
alias composer="php -d memory_limit=-1 /usr/local/bin/composer"
alias grab-site="wget -m -E -k -p -e robots=off"
alias gitlog-to-csv="git log --pretty=format:%ad,%an,%ae,%s --date=format:'%Y.%m.%d %H:%M:%S' > log.csv"
function create-current-folder-certificate {
    dir=$(pwd);
    folderName=$(basename "$dir");
    echo "$folderName";
    openssl req -x509 -nodes -days 365 -sha256 -subj "/C=CH/ST=St.Gallen/L=St.Gallen/CN=$folderName.memo" -newkey rsa:2048 -keyout "/usr/local/etc/httpd/$folderName.key" -out "/usr/local/etc/httpd/$folderName.crt";
    echo "Use VHost $folderName" >> /Users/mac/Sites/vhosts.conf;
}
export -f create-current-folder-certificate
function cdb {
    wd=$(pwd)
    folderName=$(basename "$wd")
    echo "create db $folderName"
    mysql -uroot -p -e "CREATE DATABASE $folderName"
}
export -f cdb
